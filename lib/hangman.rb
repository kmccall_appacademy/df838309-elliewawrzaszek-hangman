class Hangman
  MAX_GUESSES = 9
  attr_reader :guesser, :referee, :board

  def initialize(players)
    @guesser = players[:guesser]
    @referee = players[:referee]
    @remaining_guesses = MAX_GUESSES
  end

  def setup
    word_length = @referee.pick_secret_word
    @guesser.register_secret_length(word_length)
    @board = [nil] * word_length
  end

  def play
    setup
    while @remaining_guesses >= 0
      p @board
      take_turn
      @remaining_guesses -= 1
      if won?
        p @board
        puts "Guesser wins!"
        return
      end
    end
    puts "********** OH NO! ***********"
    puts "The word was: #{@referee.require_secret}"
    puts "You lose!"
    print "
      +---+
      |   |
      O   |
     /|\\  |
     / \\  |
          |
    =========\n"
    nil
  end

  def take_turn
    guess = @guesser.guess(@board)
    response_indicies = @referee.check_guess(guess)
    update_board(guess, response_indicies)
    @guesser.handle_response(guess, response_indicies)
  end

  def update_board(guess, indicies)
    indicies.each {|idx| @board[idx] = guess}
  end

  def won?
    @board.all?
  end

end

class HumanPlayer

  def register_secret_length(length)
    puts "The word is #{length} letters long"
  end

  def guess(board)
    puts "Guess a letter"
    gets.chomp
  end

  def handle_response(guess, response_indicies)
    unless response_indicies.empty?
      puts "The letter #{guess} is at #{response_indicies.join(", ")}"
    else
      puts "The letter #{guess} is not in this word!"
    end
  end

  def pick_secret_word
    puts "Think of a word."
    puts "How long is the word you're thinking of?"
    print "> "

    begin
      Integer(gets.chomp)
    rescue ArgumentError
      puts "Enter a valid length"
      retry
    end
  end

  def check_guess(guess)
    puts "Player guessed #{guess}"
    puts "What positions does that occur at?"
    puts "Hit enter to continue if the letter is not in this word."
    gets.chomp.split(",").map{|i_str| Integer(i_str)}
  end

  def require_secret
    puts "What word were you thinking of?"
    gets.chomp
  end

end

class ComputerPlayer

  def self.player_with_dict_file(dict_file_name)
    ComputerPlayer.new(File.readlines(dict_file_name).map(&:chomp))
  end

  attr_reader :candidate_words

  def initialize(dictionary)
    @dictionary = dictionary
  end

  def require_secret
    @secret_word
  end

  def pick_secret_word
    @secret_word = @dictionary.sample
    @secret_word.length
  end

  def register_secret_length(length)
    @candidate_words = @dictionary.select{|word| word.length == length}
  end

  def check_guess(guess_ch)
    indicies = []
    @secret_word.each_char.with_index do |ch, idx|
      if ch == guess_ch
        indicies << idx
      end
    end
    indicies
  end

  def guess(board)
    freq_table = freq_table(board)
    most_frequent_letters = freq_table.sort_by {|letter, count| count}
    letter, _ = most_frequent_letters.last
    letter
  end

  def handle_response(guess, response_indicies)
    @candidate_words.reject! do |word|
      should_delete = false

      word.each_char.with_index do |letter, idx|
        if (letter == guess) && (!response_indicies.include?(idx))
          should_delete = true
          break
        elsif (letter != guess) && (response_indicies.include?(idx))
          should_delete = true
          break
        end
      end
      should_delete
    end

  end

  private

  def freq_table(board)
    freq_table = Hash.new(0)
    @candidate_words.each do |word|
      board.each_with_index do |ch, idx|
        freq_table[word[idx]] += 1 if ch.nil?
      end
    end
    freq_table
  end

end

if __FILE__ == $PROGRAM_NAME
  print "WELCOME TO HANGMAN"
  print "
    +---+
    |   |
    O   |
   /|\\  |
   / \\  |
        |
  =========\n"
  print "Guesser: Computer(yes/no)"
  if gets.chomp == "yes"
    guesser = ComputerPlayer.player_with_dict_file("lib/dictionary.txt")
  else
    guesser = HumanPlayer.new
  end

  print "Referee: Computer (yes/no)"
  if gets.chomp == "yes"
    referee = ComputerPlayer.player_with_dict_file("lib/dictionary.txt")
  else
    referee = HumanPlayer.new
  end
  Hangman.new({guesser: guesser, referee: referee}).play
end
